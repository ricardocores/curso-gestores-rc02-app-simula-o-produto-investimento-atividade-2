/**
 *
 * Nome do programa:			InvestimentoRequest.java
 * Autor:                       Ricardo Cores
 * Início de desenvolvimento:	12/06/2020
 * Descrição:					Definição do Payload da requisição da API no método POST.
 * Obs.: Não tem payload (body ou parâmetros) de requisição para GET na API.
 */

package com.cores.SimuladorInvestimento.web.dto;

import org.springframework.stereotype.Component;

@Component //fundamental para ela ser uma classe Bean
public class InvestimentoRequest {

	private double valor;
	private int quantidadeMeses;
	
	public InvestimentoRequest() {}
	
	public double getValor() {
		return valor;
	}
	
	public int getQuantidadeMeses() {
		return quantidadeMeses;
	}
	
	public void setValor(double valor) {
		this.valor				= valor;
	}
	
	public void setQuantidadeMeses(int quantidadeMeses) {
		this.quantidadeMeses	= quantidadeMeses;
	}
}
