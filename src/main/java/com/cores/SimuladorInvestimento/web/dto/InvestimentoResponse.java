/**
 *
 * Nome do programa:			InvestimentoResponse.java
 * Autor:                       Ricardo Cores
 * Início de desenvolvimento:	10/06/2020
 * Descrição:					Definição do Payload da resposta da API para GET e POST.
 * 
 */

package com.cores.SimuladorInvestimento.web.dto;

import org.springframework.stereotype.Component;

@Component //fundamental para ela ser uma classe Bean
public class InvestimentoResponse {

	//uma classe Bean precisa ter os atributos private, construtor vazio e todos getters e setters.
	//Eu esqueci de colocar um getter do montante e este valor não estava sendo enviado no payload do retorno.
	
	private double valor;
	private int quantidadeMeses;
	private double montante;
	
	public InvestimentoResponse() {} // construtor vazio - obrigatório
	
	//Retorna Valor do investimento
	public double getValor() {
		return valor;
	}
	
	//Retorna Quantidade de Meses do investimento
	public int getQuantidadeMeses() {
		return quantidadeMeses;
	}
	
	//Retorna Montante do Valor Acumulado
	public double getMontante() {
		return montante;
	}
	
	//Seta Valor do Investimento
	public void setValor(double valor) {
		this.valor					= valor;
	}
	
	//Seta Quantidade de Meses do Investimento
	public void setQuantidadeMeses(int quantidadeMeses) {
		this.quantidadeMeses		= quantidadeMeses;
	}
	
	//Seta Montante do Valor Acumulado
	public void setMontante(double montante) {
		this.montante	= montante;
	}
	
}
