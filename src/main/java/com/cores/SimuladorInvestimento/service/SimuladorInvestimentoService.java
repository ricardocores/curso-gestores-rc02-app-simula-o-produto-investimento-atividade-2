/**
 *
 * Nome do programa:			SimuladorInvestimentoService.java
 * Autor:                       Ricardo Cores
 * Início de desenvolvimento:	12/06/2020
 * Descrição:					Processa retorno de Simulações de Investimento (GET) e orquestra o cálculo de novas requisições (POST)
 * 
 */

package com.cores.SimuladorInvestimento.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cores.SimuladorInvestimento.persistence.Investimento;

@Service
public class SimuladorInvestimentoService {
	
	@Autowired
	private List<Investimento> listaInvestimentos; //já cria um item vazio na posição 0
	
	@Autowired
	private SimuladorValorInvestido simulador;
	
	public List<Investimento> getAllInvestimentos(){
		
		/* teste de inserção
		investimento		= new Investimento();
		investimento.setValorInvestimento(1000);
		investimento.setQuantidadeMeses(2);
		investimento.setMontanteAcumulado(1044.12);
		
		listaInvestimentos.add(investimento);
		
		investimento		= new Investimento();
		investimento.setValorInvestimento(2000);
		investimento.setQuantidadeMeses(2);
		investimento.setMontanteAcumulado(2088.24);
		
		listaInvestimentos.add(investimento);
		*/
		
		return listaInvestimentos;
		
	}
	
	public Investimento simularInvestimento(Investimento investimento) {
		
		
		investimento.setMontanteAcumulado(simulador.simularInvestimento(investimento));
		listaInvestimentos.add(investimento);
		
		return investimento;
	}
	
}
