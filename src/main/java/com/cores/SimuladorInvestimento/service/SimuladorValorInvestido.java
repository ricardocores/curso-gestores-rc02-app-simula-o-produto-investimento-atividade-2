/**
 *
 * Nome do programa:			SimuladorValorInvestido.java
 * Autor:                       Ricardo Cores
 * Início de desenvolvimento:	17/05/2020
 * Descrição:					Simula o valor investido pelo cliente após "n" meses (Juros compostos)
 */

package com.cores.SimuladorInvestimento.service;

import org.springframework.stereotype.Component;

import com.cores.SimuladorInvestimento.persistence.Investimento;

@Component //anotação necessária para executar Autowired (auto-instancia na subida do Spring Boot)
public class SimuladorValorInvestido {
	
	double valorAcumulado			= 0;
	
	public double simularInvestimento(Investimento investimento) {
		
		ProdutoInvestimento produto		= new ProdutoInvestimento();
		
		valorAcumulado					= investimento.getValorInvestimento();
		
		for (int i = 0; i < investimento.getQuantidadeMeses(); i++) {
			valorAcumulado			    = (valorAcumulado * (produto.getTaxaInvestimento() + 1));
		}
		
		return valorAcumulado;
	}
}
