/**
 *
 * Nome do programa:			AppSimuladorInvestimento.java
 * Autor:                       Ricardo Cores
 * Início de desenvolvimento:	12/06/2020
 * Descrição:					Aplicação Web de Simulação de Produto de investimento com taxa de rendimento de 0,7% / mês, utilizando Spring Boot, sem persistência em banco de dados.
 * 
 */

package com.cores.SimuladorInvestimento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AppSimuladorInvestimento {

	public static void main(String[] args) {
		SpringApplication.run(AppSimuladorInvestimento.class, args);

	}

}


/* Como executar:

1) Para realizar uma simulação de Investimento

	- Fazer um POST via POSTMAN - localhost:8080/investimento
	- Injetar - aba BODY - escolher formato JSON
		{
			"valor" : "1000.00",
			"quantidadeMeses" : "2"
	    }

2)consultar todas simulações - localhost:8080/investimento (Pelo browser ou Postman)
  
*/