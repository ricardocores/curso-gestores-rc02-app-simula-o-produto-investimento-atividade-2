/**
 *
 * Nome do programa:			Investimento.java
 * Autor:                       Ricardo Cores
 * Início de desenvolvimento:	12/06/2020
 * Descrição:					Entidade contendo as informações da simulação do Investimento.
 * 
 */

package com.cores.SimuladorInvestimento.persistence;


import org.springframework.format.annotation.NumberFormat;
import org.springframework.stereotype.Component;

@Component
public class Investimento {

	@NumberFormat
	private double valorInvestimento;
	
	@NumberFormat
	private int quantidadeMeses;
	
	private double montanteAcumulado;
	
	public Investimento() {} //construtor vazio - obrigatório
	
	public double getValorInvestimento() {
		return valorInvestimento;
	}
	
	public int getQuantidadeMeses() {
		return quantidadeMeses;
	}
	
	public double getMontanteAcumulado() {
		return montanteAcumulado;
	}
	
	public void setValorInvestimento(double valor) {
		valorInvestimento				= valor;
	}
	
	public void setQuantidadeMeses(int qtdeMeses) {
		quantidadeMeses					= qtdeMeses;
	}
	
	public void setMontanteAcumulado(double montante) {
		montanteAcumulado				= montante;
	}
	
}
